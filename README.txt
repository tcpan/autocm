PROJECT DESCRIPTION

This project modifies the CancerMath.net calculator implementation to provide 3 primary functions:

1. to allow extraction of internal variables that contain calculated cancer survival probability and mortality risks
2. to allow interpolation of the annual data at monthly granularity
3. to allow commandline invocation and automated processing of large number of patient samples.

AUTHOR
Tony Pan.  tcp1975@gmail.com

FUNDING
This project was initiated with funding from William Read, MD, and Jeffrey Switchenko, PhD.



PROJECT REQUIREMENTS

The project has as its primary requirements the 3 functions named above.  In addition, the folowing constraints are raised.

The project aims to maintain as much of the logical implementation of the original CancerMath.net algorithm implementations,
and to provide a relatively easy to install, easy to use, and easy to maintain tool.  The validity of the results needs to be
consistent with that of the CancerMath.net nomogram calculators.

A second constraint is to make the project as generalizable as possible.  Initial implementation targets Breast Cancer,
Therapy based nomogram calculator.  The code should with little effort be adapted to work with other nomogram calculators
for other diseases. 


SOFTWARE DESIGN

Based on these constraints, the approach taken was to maintain the use of javascript language, and as much of the original
javascript implementation as possible.  This is accomplished by web extraction of the original html source code and transformation
of this code into a pure javascript module.  All presentation related codes are commented out, and the local variable are converted
into module-global variables for external access.  

All transformations are performed using an automated javascript script,
prepareCalc.js, running inside Node.js as a serverside/desktop process.   The automation, and the minimal modifications to the
original logic, helps to ensure that the transformation script can adapt to other nomogram calculators.

The transformed javascript module is then used in a second script, execute.js. This script allows a CSV file containing multiple
subjects' information to be read and processed in an automated way.  Each subject's data is read and then transformed into a
format that is compatible with the CancerMath nomogram calculators.  The desired internal calculator data is retrieved and stored
int a new CSV file in both yearly and monthly timesteps.  

In this step, a configuration file is used to configure the mapping between
data type and values in the CSV file to those expected by the CancerMath calculators.  The configuration file is manually edited,
and provides a powerful mechanism to allow different types of source datasets to map to different nomogram calculator implementations.


EXTENSIBILITY
1. We can upgrade to new versions of CancerMath calculators with small amount of changes in our code
2. We can use different CancerMath calculators with small amount of changes in our code and in our input CSV format (headers)
3. We can use different data but editing a mapping configuration file.

FILES

The project has the following hierarchy:

src/	- prepareCalc.js
	- execute.js
config/	- breastcancer-therapy-mapping.template.json
README.txt
configure.bat
configure.sh
CM/	  this is created by configure.bat/configure.sh scripts, and is used to store the downloaded and transformed javascript modules.
data/	  this is created by configure.bat/configure.sh scripts, and can be used to store the CSV data.


The purpose of prepareCalc.js and execute.js are described above.

config/breastcancer-therapy-mapping.template.json is an example mapping configuration file.  It serves as an example and a template
for users to develop additional mappings.  Please see the MAPPING CONFIGURATION section for more information.

configure.bat and configure.sh set up the project directory and Node.js with the relevant 3rd party modules for this project.

README.txt is this file.




DOWNLOAD

The source code is available at https://bitbucket.org/tcpan/cancermath/overview
There is a link to download, on the right hand side.  Alternative, the source code can be checked out by git.

The repository is currently non-public.  If you are interested in getting or contributing to the software, please contact me at tcp1975@gmail.com, or Dr. William Read at Emory University.




INSTALLATION

Download the code from bitbucket.org (above) and unpack into your local directory.

Requirement:  the primary requirement for this project is Node.js.  We need version 0.10.0 or later.

Windows: 
1. install Node.js from http://nodejs.org. 32 bit or 64 bit should both work.
2. open a command window (from StartMenu/Accessories), navigate to where you've downloaded the project
3. 	configure.bat

this will install the node.js packages 'optimist' and 'lazy', as well as creating the CM and data directories.

Note that on Windows, if the Microsoft HPC Pack is installed, there is already a command called "node" that differs from Node.js's node.exe.
To use Node.js' node.exe, the full path can be supplied:  c:\Program Files\nodejs\node.exe

Mac OSX
1. install Node.js from http://nodejs.org/download/. 64 bit should be fine.
2. open terminal (use spotlight, type "terminal", press Enter.), navigate to where you've downloaded the project
3.     configure.sh

Linux
1. The easiest way to install node.js is to follow directions from https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager.
Note that we need node.js version 0.10.0 or later.
2. open a terminal, and navigate to where you've downloaded the project
3. 	configure.sh





USAGE

Both commands are run from the commandline.  to open a commandline window, 

Windows:
	open a command prompt (from StartMenu/Accessories), navigate to where you've downloaded the project

Linux:
	start a terminal following the particular linux distribution's approach.
	IMPORTANT: due to a naming conflict, the executable for node.js on linux is called nodejs instead of node.

Mac:
	use spotlight, search for terminal, and start it.





DOWNLOAD AND TRANSFORM

(IMPORTANT - on linux, call nodejs instead of node)

To download and transform a CancerMath nomogram calculator, run
	node src\prepareCalc.js -c breastcancer -t therapy


To see what options are available and to read their descriptons,
	node src\prepareCalc.js -h


currently, the following are tested to work for the entire workflow
	breastcancer + therapy

The following appears to work for the extraction stage
	breastcancer + outcome
	breastcancer + condsurv
	melanoma + outcome
	melanoma + condsurv
	renalcell + outcome
	coloncancer + outcome
	headneck + outcome

The following are confirmed to fail
	breastcancer + nodal
	breastcancer + nipplecalc
	melanoma + nodal



PROCESSING DATA

Step 1, the data should be in CSV format, with the first line being the column headers, first column being the patient id, and 1 row per patient.
columns 7 and 8 (start counting at 0) should contain the deceased status and survival time, respectively.

Step 2 please review CM/{cancerType}-{calculatorType}.json for a list of available variables for the calculator of choice.

Step 3 involves making a copy of config/breastcancer-therapy-mapping.template.json, and modify it to 
	1. match your data's specification, including column names, and value mapping function or lookup table.
	2. match your calculator's variable names
	3. mark the parameters that you want to calculator to use (remaining are set to "unknown")

Step 4 review available internal variables to extract
	node src/execute.js -h

Step 5 invoke the tool with 
	node src/execute.js -c breastcancer -t therapy -p your_mapping_file -v chosen_internal_variable -i inputCSV -o outputCSV

This will invoke the calculator with the mapped/transformed values for each patient, and extract the 15 year annual data and compute the 180month interpolated data.
2 output files are generated, one a yearly report and one a monthly report.  We do not truncate to the subject's survival duration.

Note:  the code does not try to make missing directory.  Also it does not warn before overwriting an existing file.



TESTING

Testing is conducted by comparing the web generated graph and corresponding data to those computed by the automated process.  5 random samples are used to testing.

The following computation is confirmed to work
	breastcancer + therapy


The following computation runs but results are not verified.
	breastcancer + outcome (change mapping config file to exclude chemotherapy)
	
	the following is confirmed NOT to work
	breastcancer + condsurv.  (variables specific to conditional survival are present, and missing recurrence data.)
	renalcell + outcome.  (variable name mapping issue).




MAPPING CONFIGURATION FILE

This file is in JSON format, which represents name-value pair data in hierarchical form.  An example is config/breastcancer-therapy-mapping.template.json.
This file is constructed by hand because there is a significant amount of variability that is hard to generalize: on the cvs heading side, on the CancerMath internal calculator variable declarations, and in the parameters chosen for an experiment.

A template is availabel in data subdirectory to help with constructing additional mapping files.   You can also refer to the CM/{cancertype}_{calculatorType}.json for a list of CancerMath calculator variables.



In general, this file looks like

{
	"csv_col_name" : {
		"varname" : "calc var name",
		"cancermath" :  "true",
		"selected" : "false",
		"valuemap" : {
			"lookup" : {
				"csvVal": "calcVal",
				...
			}
		}
	},
	
	....
	"csv_col_name" : [
	{
		"varname" : "calc var name",
		"cancermath":  "false",
		"selected": "false",
		"valuemap": {
			"func": "function name"
		}
	},
	{
		"varname" : "calc var name",
		"cancermath":  "true",
		"selected": "true",
		"valuemap": {
			"lookup": {
				"csvVal" : 10,
				...
			}
		}
	}
	],
	....
	
}

For JSON files, all strings are quoted. Only numbers and boolean values are not quoted.  commas are used to separate items at the same hierarchical level.
More information about JSON format can be seen here: http://www.w3schools.com/json/json_syntax.asp


note that if a csv column affects more than 1 variables in the array, the 2 are grouped together via [ ], with comma separation. This is an array.

the meaningfo the field are as follows
	varname:  name of the calculator variable
	cancermath:  if the variable is use by the CancerMath calculator.  If not, then it may be used internally, for example, months of survival and seq (id)
	selected:  if the variable is a calculator variable, selected determines if we want to pass that variable to the calculator.
	valuemap: it is used to map discrete values in the csv to discrete values allowed by the CancerMath calculator, using a "lookup" table..
alternatively, it can specify a function to apply to the values.  for example, rounding a floating point number to nearest integer would use "round".
The use of lookup tables gives the user flexibility, while the "func" based approach allows for more complex transformation.
note that the function is restricted to unary, i.e. takes only the value as its parameter, and return a single value.
	y = func(x)


The current execute.js code supports "round", "asNumeric", "notEmpty", "equateEmptyToZero".  Others may be added in execute.js in the future.

For convenience, if the CSV column name is the same as the variable name in the CancerMath calculator, the "varname" field can be omitted.
similarly, valuemap can be omitted if the allowable values in csv are the same as those in the CancerMath calculator.

Note: for breast cancer, the template configuration file maps all treatment arms to CA*4.  This is to create a phantom trial arm for statistical analysis purpose.  Also, all ER positive subjects are automatically set to have endotherapy with Tamoxifen.






