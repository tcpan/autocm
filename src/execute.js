/*
 * Copyright 2013 Tony Pan, William Read, Jeffrey Switchenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
Author:  Tony Pan

this script is the "driver" for automated analysis using the CancerMath logic.

it loads the previously generated CancerMath javascript module (extracted and transformed from website, via prepareCalc.js)
loads the corresponding json file, which is a template containing all the parameters used by the calculator,
and loads the use specified json mapping configuration file (constructed by hand.  see template in config/ directory.)

then it loads a CSV file with the subjects in rows and parameters in columns

for each row, 
	transform the values and populate a copy of the parameter template.
	call the CancerMath calculator
	get the target output variable
	save out to a new csv file
	
type "node execute.js" for commandline parameters.	

*/


// parsing the commandline parameters.
var opsobj = require('optimist')
	.usage('Execute the specified CancerMath calculator in batch mode.\nUsage: $0')
	.describe('c', 'cancer type to work with.\n\t\tDEFAULT=breastcancer.\n\t\tsupported:\n\t\t\tbreastcancer\n\t\t\tmelanoma\n\t\t\trenalcell\n\t\t\tcoloncancer\n\t\t\theadneck')
	.describe('t', 'calculator type to work with.\n\t\tDEFAULT=therapy.\n\t\tsupported:\n\t\t\ttherapy\n\t\t\toutcome\n\t\t\tcondsurv')
	.describe('p', 'REQUIRED. file containing the CSV columns to include during calculation.  by default it shares the same filename prefix as input file but in data subdir')
	.describe('v', 'REQUIRED. variable we are interested in calculating.  To see a list of supported variables, run with -h')
	.describe('i', 'REQUIRED. name of csv input file, one subject per row')
	.describe('o', 'REQUIRED. name of csv output file, one subject per row')
	.describe('h', 'this help function')
	.alias('c', 'cancer')
	.alias('t', 'calc')
	.alias('p', 'params')
	.alias('v', 'variable')
	.alias('i', 'input')
	.alias('o', 'output')
	.alias('h', 'help')
	.boolean('h');
var ops = opsobj.argv;
	
if (ops.h || !ops.i || !ops.o || !ops.p || !ops.v) {
	opsobj.showHelp();
	
	console.log("Supported variables to extract for the -v parameter\n");
	console.log("\tnvsr_death_prob_yearly");
	console.log("\tL_breastcancer_distribution");
	console.log("\tnvsr_life_expect");
	console.log("\tL_breastcancer_percentage");
	console.log("\tL_nonbreastcancer_prob");
	console.log("\tL_breastcancer_death_yearly");
	console.log("\tL_cancer_death_cumm");
	console.log("\tL_nonbreastcancer_death_yearly");
	console.log("\tL_noncancer_death_cumm");
	console.log("\tL_overall_death_yearly");
	console.log("\tL_overall_death_cumm");
	console.log("\tremaining_percentage");
	console.log("\tcancer_death_hazard");
	console.log("\tcancer_death_dist_cumm");
	console.log("\tL_cancer_percentage_therapy");
	console.log("\tL_noncancer_prob_therapy");
	console.log("\tL_cancer_death_yearly_therapy");
	console.log("\tL_cancer_death_cumm_therapy");
	console.log("\tL_noncancer_death_yearly_therapy");
	console.log("\tL_noncancer_death_cumm_therapy");
	console.log("\tL_overall_death_yearly_therapy");
	console.log("\tL_overall_death_cumm_therapy");
	console.log("\tremaining_percentage_therapy");
	
	process.exit(0);
}

var path = require('path');

var cancerType = ops.c;
var calcType = ops.t;
var paramfile = path.resolve(ops.p);
var targetVar = ops.v;
var inputfile = path.resolve(ops.i);
var outputfile = path.resolve(ops.o);

console.log("Parsed commandline parameters");

var fs = require('fs');

var js_file = path.resolve(__dirname,  '..', 'CM', cancerType + '_' + calcType + '.js');
var json_file = path.resolve(__dirname,  '..', 'CM', cancerType + '_' + calcType + '.json');

/* jsdom modification of the template html (without javascript) does not work well.
	there is no easy way of setting form data?
// load the html file  (synchronously, else we have problems with working with the dom object.
var htmlcontent = fs.readFileSync(__dirname + '/../CM/' + cancerType + '_' + calcType + '.html', 'utf8');

// set up the html to dom parser and handler.  We need a document object to send to updateGraph
var jsdom = require('jsdom');
jsdom.env({
	html: htmlcontent,
	scripts: [
		'http://code.jquery.com/jquery-1.5.min.js'
	],
	done:  function(err, window) {
		var $ = window.jQuery;
		var $form = $('#infoForm').firstChild;
		
		// now try populating it a bit
		var doc = window.document;
		console.log(doc.getElementById("death_reduction").firstChild.nodeValue);
		console.log(doc.getElementById("nodesKnown").value);

		var $ = window.jQuery;
		console.log($('#nodesKnown'));


		// now try retrieving the values again.
	}

});
*/


/*  zombie does not work well - it's not clear what happens when we have a file url instead of a http url
	browser.document does not appear to populate, so can't change the form data.
	
	server return status 404 from the file...

var path = require("path");
var htmlfile = path.resolve(__dirname + '/../CM/' + cancerType + "_" + calcType + ".html");
var htmlurl= "file://" + htmlfile;
var htmlcontent = fs.readFileSync(htmlfile);


var Browser = require("zombie");
var browser = Browser.visit(htmlurl, {runScripts: false, loadCSS: false, debug: false},
	function(e, browser, status) {
		console.log(browser.document);
});


//var b2 = new Browser({runScripts: false, loadCSS: false, debug: false});
//b2.load(htmlcontent);
*/


// load the calculator module
var cm = require(js_file);

console.log("loaded calculator module: " + js_file);

// load the parameter template in json format
var jsondata = fs.readFileSync(json_file, 'utf8');
console.log("loaded calculator variable template: " + json_file);

////////////
// and configure the template with default values from the CancerMath html form.
// default values used by the template generator (prepCalc.js) are value=0, selected=false
// below we are just modifying application specific defaults.	

// this would be SPECIFIC for CancerMath Calculators.
// actually, DEFAULT VALUES of 0 and selected of false ARE FINE.

////////////
// then we load the csv to parameter mappings 
var mappingcontent = fs.readFileSync(paramfile, 'utf8');
var mappings = JSON.parse(mappingcontent);

//for (var key in mappings) {
//	console.log(key);
//}

console.log("Loaded mapping configuration " + paramfile);

///////// define some commonly used mappings functions
// rounding
var round = function(x) {
	return Math.round(x);
}
// convert from string to number
var asNumeric = function(x) {
	return x * 1;
}
// returns 1 if input is not empty, else 0
var notEmpty = function(x) {
	if (!x || 0 == x.length) return 0;
	else return 1;
}
// returns 0 if input is empty, else return original value
var equateEmptyToZero = function(x) {
	if (!x || 0 == x.length) return 0;
	else return x;
}


console.log("Set up value mapping functions used by CSV to CancerMath variable mapping");




/////// create logic for mappings the value from csv to CancerMath variables.

var firstline = true;
var badmapping = false;
var headings = null;


// perform lightweight validation of the mapping configuration file
var isMappingOk = function (heading, m) {
	if ('valuemap' in m) {
		if ('func' in m.valuemap) {
			// if func is specified, has to be a valid and known function.
			if (m.valuemap.func == 'round') {
			} else if (m.valuemap.func == 'asNumeric') {
			} else if (m.valuemap.func == 'notEmpty') {
			} else if (m.valuemap.func == 'equateEmptyToZero') {
			} else {
				console.log('ERROR: unknown ' + heading + ' mappings function ' + m.valuemap.func + ' for ' + paramfile);
				return false;
			}
		} else if ('lookup' in m.valuemap) {  // lookup is always okay.
		} else {  // neither lookup nor func - not supported
			console.log('ERROR: unkown value mapping approach ' + heading + ' for ' + paramfile);
			return false;
		}
	}  // okay for valuemap not to be present
	return true;
}

// perform mapping from CSV values to CancerMath compatible values.
// the first line of the CSV is assumed to be a header line. The Configuration is validated during this time.
// subsequent lines are assumed to be data, and values converted.
var map = function (heading, m, v, o) {
	var output = o;
	
	// only perform the mapping for a cancermath variable that is selected to participate in the calculation.
	if (m.cancermath == true && m.selected == true) {
	
		// use a value map if there is one.
		var val = v;
		if ('valuemap' in m) {
		
			if ('func' in m.valuemap) {  // if a function is specified,
				// find the function to use.
				if (m.valuemap.func == "round")
					val = Math.round(v);
				else if (m.valuemap.func == "asNumeric")
					val = asNumeric(v);
				else if (m.valuemap.func == "notEmpty")
					val = notEmpty(v);
				else if (m.valuemap.func == "equateEmptyToZero")
					val = equateEmptyToZero(v);
				// already checked the mapping configuration.  we don't need to worry about functions that are unknown.
			} else if ('lookup' in m.valuemap) {
				val = m.valuemap.lookup[v];
			} // else we have a malformed mapping - already checked.
			
		} // else  no valuemap property, so just pass the original value along.

		// get the cancermath variable name.  If varname is not present, assume the CSV heading is same as the actual variable name.
		var vname = heading;
		if ('varname' in m) {
			vname = m.varname;
		}
		
		// can populate "value" field but not "selected" field.
		if ('value' in output.form[vname]) {
			output.form[vname].value = val;
		} else if ('selected' in output.form[vname]) {
			console.log("ERROR: not implemented.  we do not encounter this use case with the current datasets.");
		}
		
	} // else variable not used in cancermath, so ignore.
	return output;
}


// function to transform the csv input data into the form expected by the calculator
// this function uses the value mapping in the supplied param mapping file.
// varname in mapping configuration is optional.
// valuemap in mapping configuration is optional.
var paramMapping = function(tokens, firstline) {
	if (firstline) {
		// the line is header, now check to see if all the tokens are in the mappings
		for (var i = 0; i < tokens.length; i++) {
			var heading = tokens[i];
			
			// make sure the heading is in the mappings.
			if (!(heading in mappings)) {	
				console.log("ERROR: CSV column heading \"" + heading + "\" is not in the mappings file \"" + paramfile + "\"");
				badmapping = true;
			} else {
				//console.log('found \"' + heading + "\"");
				
				var mapping = mappings[heading];
				
				// if the mapping object for this heading is an array, we need to check all of them.
				if (mapping instanceof Array) {
					
					for (var j = 0; j < mapping.length; j++) {
						// check to see if we have the known value mapping functions.
						//console.log(mapping[j]);
						badmapping = badmapping || !isMappingOk(heading, mapping[j]);
					}
				} else {  // not an array, so check normally.
					badmapping = badmapping || !isMappingOk(heading, mapping);
				}
			}
		}
		if (!badmapping) {
			headings = tokens;
		}
		
		return null;
	}
	// otherwise this is not the header line.  so process it.		
	
	if (badmapping) return null;
	
	// get a new template to fill it 
	var output = JSON.parse(jsondata);

	// fill it.
	for (var i = 0; i < tokens.length; i++) {
		
		// get the heading and the corresponding mapping element.
		var heading = headings[i];
		var mapping = mappings[heading];
		
		// if the mapping object for this heading is an array, we need to map to all of them.
		if (mapping instanceof Array) {
			
			for (var j = 0; j < mapping.length; j++) {
				// map values
				output = map(heading, mapping[j], tokens[i], output);
			}
		} else {  // not an array, so map values normally.
			output = map(heading, mapping, tokens[i], output);
		}
	}	
	
	return output;
};



// do a simple super sampling.  newSampleRate has to be a positive integer. 
// so we only do upsampling.
// standard linear interpolation.
var upsampleLinearSurvived = function(input, newSampleRate, dec, survivedMonths) {
	var output = new Array();
	var f_samples = newSampleRate*1.0;
	var a, b, c, oldStep, newStep, k = 0;
	for (var i = 1; i < input.length; i++) {
		a = input[i-1] * 1.0;
		b = input[i] * 1.0;
		
		oldStep = b-a;
		newStep = oldStep / f_samples;
		for (var j = 0, c = a; j < newSampleRate; j++, c += newStep, k++) {		
			if (dec == '1=dead' && k > survivedMonths) {
				// deceased and we are not predicting more than the survived months.  so done
				return output;
			}
			output[k] = c;
		}
	}
	// add the last entry
	if (dec == '1=dead' && k > survivedMonths) {
		// deceased and we are not predicting more than the survived months.  so done
		return output;
	}
	output[k] = input[input.length - 1];
	return output;
}

// do a simple super sampling.  newSampleRate has to be a positive integer. 
// so we only do upsampling.
// standard linear interpolation.
var upsampleLinear = function(input, newSampleRate) {
	var output = new Array();
	var f_samples = newSampleRate*1.0;
	var a, b, c, oldStep, newStep, k = 0;
	for (var i = 1; i < input.length; i++) {
		a = input[i-1] * 1.0;
		b = input[i] * 1.0;
		
		oldStep = b-a;
		newStep = oldStep / f_samples;
		for (var j = 0, c = a; j < newSampleRate; j++, c += newStep, k++) {		
			output[k] = c;
		}
	}
	// add the last entry
	output[k] = input[input.length - 1];
	return output;
}


console.log("Set up auxillary functions for processing");



// get the yearly output file
var writeYearly = fs.createWriteStream(outputfile.replace(/\.csv$/, "") + ".yearly.csv");

console.log("Opened yearly data file for write " + outputfile.replace(/\.csv$/, "") + ".yearly.csv");


// get the monthly output file
var writeMonthly = fs.createWriteStream(outputfile);

console.log("Opened monthly data file for write " + outputfile);

 
// now load the CSV source data, go through it line by line
// for each line, convert the CSV data to CancerMath format, and call the CancerMath function
// 		get the target variable output, and store into output CSV file.
var lazy = require('lazy');
var params;
var tokens;
var result;
var result2;
var count = 0;
// processing the CSV 1 line at a time
new lazy(fs.createReadStream(inputfile))
	.lines
	.forEach(function(line) {
	
		// trim the line of white spaces at the ends, then split.
		tokens = line.toString().replace(/^\s+|\s+$/g, "").replace(/\s*,\s*/g, ",").split(",");
		
		// populate the parameters json object.  treat the first line differently (as headers)
		params = paramMapping(tokens, firstline);
		if (firstline) {

			writeYearly.write(tokens.join(","));
			for (var i = 0; i <= 15; i++) {
			        writeYearly.write(",y" + i);
			}
			writeYearly.write("\n");


			writeMonthly.write(tokens.join(","));
			for (var i = 0; i <= 15*12; i++) {
			        writeMonthly.write(",m" + i);
			}
			writeMonthly.write("\n");

			firstline = false;
		}
		//console.log(params);
		
		// compute the nomogram if parameters are okay.
		if (params != null) {
			
			// compute the 15 year annualized nomogram.
			cm.updateGraph(params);
			
			// get the internal variable that the user asked for
			result = cm.get_var(targetVar).join(",");
			// and write the line out
			writeYearly.write(tokens.join(",") + "," + result + "\n");
			
//			// compute the 180 monthly nomogram, and store it.  truncate at maximum survival, unless the patient is still alive.
//			deceased = tokens[7];
//			survival = Math.round(tokens[8]);  // round to nearest month for survival.

//			result2 = upsampleLinearTruncated(cm.get_var(targetVar), 12, deceased, survival);
			result2 = upsampleLinear(cm.get_var(targetVar), 12);
			writeMonthly.write(tokens.join(",") + "," + result2 + "\n");
		
			if (count < 2) console.log(params);
	
			count++;
			if (count % 100 == 0) {
				console.log(count + " subjects processed");
			}
		}
	});

console.log("Began reading input file and processing.  Will return to prompt when done.");
