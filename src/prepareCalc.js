/*
 * Copyright 2013 Tony Pan, William Read, Jeffrey Switchenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
Author: Tony Pan

this javascript downloads the CancerMath web calculator html/javascript source, and process it into 2 separate files
the first is a javascript file that can be loaded as a javascript module.  This contains all the core logic.
the second is a JSON file that is a template for all the calculator's parameters.


This minimizes the amount of changes to the original javascript.  The following changes are made to the input web page source
1. only javascript blocks containing "function" declarations are kept.
2. "<script>" and "</script>" tags are removed.
3. all function local variable declarations are changed to module local.  this is so we can access the intermediate data for export
		this MAY have side effects if there are variables of the same name:  global local match, or local-local match
		IN CURRENT CANCERMATH IMPLEMENTATION THIS IS UNLIKELY.
			duplicate local-local variable conflicts should be detectable at run time: 2 global var declarations of the same name.
			global-local variable conflicts should also be detectable if global variable uses "var" keyword.
			global-local variable conflicts are hard to detect if global var does not use var keyword.
			
		VERIFICATION needs to be done by plotting the uninterpolated results and compare to web calc plot.
	the module local variables also get a new accessor function to retrieve the variables by name.
	
4. updateGraph function signature changed to take in an html document  (calculator parameters)
5. plotGraph and other javascript line that are intended to generate or modify browser rendering of the html page are commented out.
6. functions declaration are reformatted so they can be exported as module functions.


type "node execute.js" for commandline parameters.	

*/

// parsing commandline parameters.
var opsobj = require('optimist')
	.usage('Download and process the CancerMath web calculator.\nUsage: $0')
	.alias('c', 'cancer')
	.alias('t', 'calc')
	.alias('f', 'func')
	.alias('h', 'help')
	.describe('c', 'REQUIRED. cancer type to work with.\n\t\tsupported:\n\t\t\tbreastcancer\n\t\t\tmelanoma\n\t\t\trenalcell\n\t\t\tcoloncancer\n\t\t\theadneck')
	.describe('t', 'REQUIRED. calculator type to work with.  Not all cancer types has all calculators\n\t\tsupported:\n\t\t\ttherapy\n\t\t\toutcome\n\t\t\tcondsurv')
	.describe('f', 'name of the main calculator function if known.\n\t\tDEFAULT=updateGraph\n\t\tfor breastcancer, therapy, this is updateGraph')
	.describe('h', 'this help function')
	.boolean('h');
var ops  = opsobj.argv;
	
if (ops.h || !ops.c || !ops.t) {
	opsobj.showHelp();

	process.exit(0);
	
}

var cancerType = ops.c;
var calcType = ops.t;
var funcName = ops.f;


console.log("Processed commandline parameters"); 

var path = require('path');
var js_file = path.resolve(__dirname,  '..', 'CM', cancerType + '_' + calcType + '.js');
var json_file = path.resolve(__dirname,  '..', 'CM', cancerType + '_' + calcType + '.json');
var src_file = path.resolve(__dirname,  '..', 'CM', cancerType + '_' + calcType + '.src.html');


// load required library
var http = require("http");
var fs = require("fs");


// specify page to retrieve  - function like wget.
var options = {
	host: 'www.lifemath.net',
	port: 80,
	path: '/cancer/' + cancerType + '/' + calcType + '/index.php'
};

console.log(options);

//  get only the unique variables declarations.  used during parameter template generation.
function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

// function to transform the downloaded xhtml+javascript source file from CancerMath.
var processHTML = function(data) {

	////////// find the javascript blocks in the original html.
	
	// now extract the important javascript sections that contain functions.  this is done by searching for 1 or more keywords.
	// matching substring starting with "<script",
	// followed by 0 or 1 space "\s?"
	// followed by some text (e.g. attributes for <script>) but not end bracket in a non-greedy way, "[^>]*?"
	// followed by 1 or more "(...)+" chars (including EOL) "[\s\S]" but not allowing </script> "(?!<\/script>)"
	// followed by a function declaration "(function\s+[a-zA-Z]+\s*\()"
	// followed by 1 or more "(...)+" chars (including EOL) "[\s\S]" but not allowing </script> "(?!<\/script>)"
	// followed by </script> "<\/script>"
	var js_func_pattern2 = /<script\s?[^>]*?>((?!<\/script>)[\s\S])*?(function\s+\w+\s*\()((?!<\/script>)[\s\S])*?<\/script>/g;
	var javascripts = data.match(js_func_pattern2);
	
	if (javascripts == null) {
		console.log("ERROR with downloading.  retrieved\n" + data);
		return;
	}
	
	var results = new Array();
	
	/////////// convert the local variables in the functions to variables that are module local in scope, and adding accessor functions
	///////////  so we can extract the computed arrays.
	
	
	// for each match, convert local vars to global.  reason is we want to be able to extract whatever values.
	for (var i = 0; i < javascripts.length; i++) {


		var content = javascripts[i];
		// first, remove the  <script> and </script> tags
		var js_tag_open_pattern = /<script\s?[^>]*?>/g;
		var js_tag_close_pattern = /<\/script>/g;
		content = content.replace(js_tag_open_pattern, "");
		content = content.replace(js_tag_close_pattern, "");


		// no good way of extracting just a function then extract local vars only.
		// and we can't technically omit "var" keyword everywhere
		// so do this:
		// 1. get all var names.
		// 2. remove all "var" keywords
		// 3. prepend the functions with "var name" list.
		
		var varnames = new Array();
		
		// get the var declarations.  either we have var name; or var name =...;
		var vardecl_pattern = /\s*var\s+\w+[\s\S]*?\;/g;
		var vardecls = content.match(vardecl_pattern);
		
		// get ready to extract just the name
		var varname_pattern = /\s*var\s+(\w+)[\s\S]*?\;/;
		
		if (vardecls != null) {
			
			// for each declaration, get the name of the variable and add to array
			for (var j = 0; j < vardecls.length; j++) {
				var varname = vardecls[j].match(varname_pattern);
				
				// save the extracted var name (capture group 1).
				varnames[j] = "var " + varname[1] + ";";
			}

			// remove all var keywords in original content
			var varkeyword_pattern = /\n\s*var\s+/g;
			var novardecl = content.replace(varkeyword_pattern, "\n");
			//console.log(result);
			
			// generate the accessor.  this is "get_var(varname)"
			var accessor = varnames.join("\n");
			accessor = accessor.replace(/var (\w+);/g, "\tif (varname == '$1') return $1;");
			accessor = "var get_var = function(varname) {\n" + accessor + "\n\tconsole.log('ERROR: unknown variable ' + varname);\n\treturn null;\n};\n";
			//console.log(accessor);	
			
			// prepend the new var declaration and accessor functions
			results[i] = varnames.join("\n") + "\n" + accessor + "\n" + novardecl;
			
		} else {
			// no vars to change to global scope so all set.
		
			results[i] = content;
		}

	}
	
	var newContent = results.join("\n");
	
	//////// this part comments out any html/browser presentation code in the javascript code
	
	// Comment out the code that puts calculated values back into the html (for browser rendering).
	// this is GENERAL
	var document_pattern = /(\s*)(document\.getElementById\("\w+"\)\.\w+\.nodeValue\s*\=)/g;
	newContent = newContent.replace(document_pattern, "$1//$2");
	document_pattern = /(\s*)(document\.getElementById\("\w+"\)\.style\.visibility\s*\=)/g;
	newContent = newContent.replace(document_pattern, "$1//$2");
	
	// Comment out the code for disclaimer popup
	// this is GENERAL
	var tb_show_pattern = /(\s+)tb_show\(/g;
	newContent = newContent.replace(tb_show_pattern, "$1//tb_show(");
	
	// Comment out alertOldText, which provides status warning messages and color changes in the html
	// this is SPECIFIC for CancerMath Calculators
	var alert_pattern = /(\s*)(alertOldText=document\.getElementById\("alertmess"\)\;)/g
	newContent = newContent.replace(alert_pattern, "$1//$2");
	alert_pattern = /(\n\s*)alertOldText\./g;
	newContent = newContent.replace(alert_pattern, "$1//alertOldText.");
	alert_pattern = /(\n\s*)(if +\( *alertOldText\.[^{]*{[^}]*}\s+else\s+{[^}]*})/g;
	newContent = newContent.replace(alert_pattern, "$1/*\n$2\n*/");

	// Comment out the updateGraph counter if-then statement
	// this is SPECIFIC for CancerMath Calculators
	var first_show_pattern = /(\n\s*)(if +\(.*?updateGraph\.counter[^{]*{[^}]*})/g;
	newContent = newContent.replace(first_show_pattern, "$1/*\n$2\n*/");

	// Comment out the plotgraph line
	// this is SPECIFIC for CancerMath Calculators
	var plotGraph_pattern = /(\s*)plotGraph\(/g;
	newContent = newContent.replace(plotGraph_pattern, "$1//plotGraph(");


	//////// modify the javascript functions to make a javascript module with callable functions.
	
	// change document.getElementById('nodesKnown').value to document.form.nodesKnown.value)
	// this is SPECIFIC for CancerMath Calculators
	var nodesKnown_pattern = /document\.getElementById\('nodesKnown'\).value/g;
	newContent = newContent.replace(nodesKnown_pattern, "document.form.nodesKnown.value");
		
	// change updategraph function signature to add "document"
	// this is SPECIFIC for CancerMath Calculators
	var updateGraph_pattern = /(\s+)updateGraph\s*\(\)/g;
	newContent = newContent.replace(updateGraph_pattern, "$1updateGraph(document)");
	var getTNM_Stage_pattern = /(\s+)getTNM_Stage\s*\(\)/g;
	newContent = newContent.replace(getTNM_Stage_pattern, "$1getTNM_Stage(document)");
	
	// convert from RTPCR[1] and RTPCR[2] to RTPCR1 and RTPCR2
	// this is SPECIFIC for CancerMath Calculators
	newContent = newContent.replace(/\.RTPCR\[1\]\./g, ".RTPCRneg.");
	newContent = newContent.replace(/\.RTPCR\[2\]\./g, ".RTPCRpos.");
	
	
	// convert from function name(..) to export.name = function(..).  This exports the functions
	// this is GENERAL
	var func_pattern = /(\n\s*)function\s+(\w+)\s*\(/g;
	newContent = newContent.replace(func_pattern, "$1var $2 = function(");
	
	// then export all of the functions.
	// this is GENERAL
	func_pattern = /\n\s*var\s+\w+\s*=\s*function\s*\(/g;
	var func_names = newContent.match(func_pattern).join("\n");
	func_names = func_names.replace(/\n\s*var\s+/g, "");
	func_names = func_names.replace(/\s*=\s*function\s*\(/g, "\n");
	func_names = func_names.replace(/(\w+)/g, "\t$1 : $1,");
	func_names = func_names.replace(/,\s+$/, "");
	func_names = "{\n" + func_names + "\n}";
	newContent = newContent + "\nmodule.exports = " + func_names + "\n";
	
	
	//save the results into a javascript module file.
	fs.writeFile(js_file, newContent, function(err2) {
		if(err2) {
			console.log(err2);
		} else {
			console.log("The javascripts were extracted and saved as " + js_file);
		}
	});
	
	
	///////// take the inputs in the javascript and create a json template for populating the parameter for
	/////////  when invoking the updateGraph function
	
	
	// extract all the document.form.{varname}.blah and save to a file as a json template
	// this new file will be loaded during calls, and used to hold parameters.
	// this is GENERAL for any javascript functions that get its input from  form data
	var var_pattern = /document\.form\.\w+\.\w+/g;
	var formvars = newContent.match(var_pattern).filter(onlyUnique);	
	// construct a json object as template.
	var document = {form: {}};
	for (var i = 0; i < formvars.length; i++) {
		var names = formvars[i].split(".");
		var temp = {};
		if (names[3] == 'value') temp[names[3]] = 0;
		else if (names[3] == 'checked') temp[names[3]] = false;
		document.form[names[2]] = temp;
	}
	fs.writeFile(json_file, JSON.stringify(document, null, 4), function(err) {
		if(err) {
		  console.log(err);
		} else {
		  console.log("JSON template saved as " + json_file);
		}
	}); 
	
	
	/*
		We are not using the html part of the web calculator so this is not generated.
	
	// extracting the non-javascript parts
	// matching any of the <scripts> tags and replace them with empty string.
	var script_pattern = /<script\s?[^>]*?>((?!<\/script>)[\s\S])*?<\/script>/g;
	var html_only = data.replace(script_pattern, "");
	// remove any noscript tags
	script_pattern = /<noscript\s?[^>]*?>((?!<\/script>)[\s\S])*?<\/noscript>/g;
	var html_only = html_only.replace(script_pattern, "");
		
	fs.writeFile(__dirname + '/../CM/' + cancerType + '_' + calcType + '.html', html_only, function(err2) {
		if(err2) {
			console.log(err2);
		} else {
			console.log("The html part was extracted.");
		}
	});
	*/
};




// open an http connection and download the source code.  this source code is kept for debugging purpose.
// when download is complete, transform the source code into javascript module and a corresponding parameter template file.
http.get(options, function(res) {
	var data = "";
	
	res.on("error", function(e) {
		console.log("Got error: " + e.message);
	});
	
	// catch the chunks of data being sent back and put them together into "data"
	res.on("data", function(chunk) {
		data += chunk;
	});
	
	// when finished, save the whole thing to a local file in CM/, and then process it to extract javascript portions
	res.on("end", function() {
		fs.writeFileSync(src_file, data);
		console.log("finished downloading.  CancerMath html file is stored as " + src_file);
		
		// now extract the javascript and html parts.  convert javascript part into a module.
		processHTML(data);
		console.log("finished processing download!");
	});
}).on('error', function(e) {
  console.log("Got ERROR: " + e.message);
});







